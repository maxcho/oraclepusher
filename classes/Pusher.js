const path = require('path');
require('dotenv').config({path: path.join(__dirname, '../.env')});
const CONF = require('../config.json');
const colors = require('colors');
colors.setTheme({
    main: 'green',
    bg: 'bgGreen',
  });
const asTable = require ('as-table').configure ({ title: x => x.main, delimiter: ' | '.main, dash: '-'.main });

const pj = require(path.join(__dirname, '../package.json'));
const AbortController = require("abort-controller");
const fetch = require("node-fetch");
const jp = require('jsonpath');
const { Eos } = require("./Eos");

const { Resource_Manager } = require("./Resource_Manager");


function parseCpuErrorMsg(msg){
    var found = [],
    rxp = /\(([^()]*)\)/g,
    curMatch;
    while( curMatch = rxp.exec(msg) ) {
        found.push( parseInt(curMatch[1]) );
    }
    return found; //[0] => billed; [1] => max availabe
}

class Pusher {

    constructor(feeds, plugins=[] ){
        // console.log('xxx', Pusher.constructor.prototype)
        this.eos = new Eos(CONF.rpc_nodes, [process.env.ACCOUNT_PK]);
        this.feeds = feeds;
        this.plugins = plugins;
        this.worker = CONF.worker || CONF.custodian;
        this.stats = {};
        this.notifiers = [];
        this.resource_manager = new Resource_Manager(this.eos, this.worker);
        this.version_check_timestamp = 0;
        this.init();

       
        
        
    }
    
    async init(){
        console.log(` ******************* `.bold.bg);
        console.log(` ORACLEPUSHER v${pj.version} `.bold.bg);
        console.log(` ******************* `.bold.bg, '\n');
        
        await this.check_version();
        this.load_plugins();
        await this.log_start_info();
        setTimeout(()=>{this.run()}, CONF.interval_ms);
    }

    async run(){
        if(this.version_check_timestamp !== 0 && Date.now() - this.version_check_timestamp >= 86400000){ //check every 24h
            await this.check_version();
        }
        this.stats["Cycle"] = this.stats["Cycle"] === undefined ? 1 : this.stats["Cycle"]+1;
        console.log(`\nCycle #${this.stats["Cycle"]}`.bold.bg);
        let quotes = await this.get_quotes(this.feeds);
        console.log(asTable(quotes));
        if(quotes.length){
            await this.push(quotes);
        }
        
        setTimeout(()=>{this.run()}, CONF.interval_ms);
    }

    async get_quotes(feeds){
        let proms = [];
        for(let i = 0; i < feeds.length; ++i){
            proms.push(this.get_quote(feeds[i]) );
        }
        return (await Promise.all(proms)).filter(q=> q.value !== null);
    }

    async get_quote(feed){
        const controller = new AbortController();
        const requestTimer = setTimeout(
          () => { controller.abort(); },
          CONF.api_request_timeout_ms
        );
        let res = null;
        try {
            let response = await fetch(feed.api_url, { signal: controller.signal });
            res = await response.json();
            if(feed.error_handler(res) ){
                this.handle_error('Feed Response Error', `${feed.error_handler(res)} ${feed.api_url}`);
                res = null;
            }
            else{
                res = jp.query(res, feed.json_path);
                res = res.length ? Math.round(feed.modifier(res[0]) ) : null;
            }
        } 
        catch(err) {
            if (err.name == "AbortError") {
                //api request timeout
                this.handle_error('Feed Request Timeout', `Request took longer then ${CONF.api_request_timeout_ms/1000} sec ${feed.api_url}`);
            }
            else {
                //other error
                this.handle_error('Feed Request Error', `${err.message} ${feed.api_url}`);
            }
        } 
        finally {
            clearTimeout(requestTimer);
            return {pair: feed.pair, value: res};
            
        }
    }


    async push(quotes){
        if(!CONF.push_to_network){
            console.log(`[Info]`.yellow, 'pushing to network disabled in config.json');
            return;
        }
        
        let push_action = {
            account: CONF.oraclehub_contract,
            name: 'push',
            authorization: CONF.authorization,
            data: {
                owner: CONF.custodian,
                quotes: quotes
            }
        }

        let pushd_action = {
            account: CONF.oraclehub_contract,
            name: 'pushd',
            authorization: CONF.authorization,
            data: {
                owner: CONF.custodian,
            }
        }
        let actions = [push_action];

        if(CONF.trigger_pushd){
            actions.push(pushd_action);
        }

        try{
            const res = await this.eos.api.transact({actions: actions}, {blocksBehind: 3, expireSeconds: 300, broadcast: true});
            if(res){
                this.stats["Success"] = this.stats["Success"] === undefined ? 1 : this.stats["Success"]+1;
                console.log(`[Success #${this.stats["Success"]}]`.green, res.processed.id);
                this.stats["EOS CPU Error"] = 0;//reset cpu error
            }
        }
        catch (e) {
            if (e instanceof this.eos.RpcError){

                switch (e.json.error.name) {
                    case "tx_cpu_usage_exceeded":
                        this.handle_error('EOS CPU Error', e.json.error.details[0].message);
                        break;
                    default:
                        this.handle_error(`EOS Error ${e.json.error.name}`, e.json.error.details[0].message);
                        break;
                }
            }
            else{
                this.handle_error('EOS Error', `${e}`);
            }
        }

    }

    handle_error(type, message){
        this.stats[type] = this.stats[type] === undefined ? 1 : this.stats[type]+1;
        console.log(`[${type} #${this.stats[type]}]`.red, message);
        //notify via telegram here;
        switch (type) {
            case 'EOS CPU Error':
                this.eos.switchNode();
                //parseCpuErrorMsg(message);
                if(this.stats[type] == 3 || this.stats[type]%20==0){ //send notification after 3 consecutive cpu errors and after each 20th
                    this.notify(`Your oraclepusher (${this.worker}) has run out of CPU during the last ${this.stats[type]} push attempts.`); 
                }
                break;
        
            default:
                break;
        }
        
    }

    async log_start_info(){
        console.log(`\n• Account Info `.underline.bold.bg);
        console.log(`Custodian:`.main, `${CONF.custodian}`);
        if(this.worker != CONF.custodian){
            console.log(`Worker:`.main, `${this.worker}`);
        }
        console.log(`Authorization:`.main, CONF.authorization.map(a => `${a.actor}@${a.permission}`).join(', ') );

        let r = await this.resource_manager.getResources(["CPU", "NET"]);
        console.log(asTable(r) );

        // let z = await this.resource_manager.getResources(["RAM"]);
        // console.log(asTable(z) );

        console.log(`\n• Feeds ${this.feeds.length} `.underline.bold.bg);
        let feeds_table = this.feeds.map((f,i) =>{
            let res = {};
            res.feed = `#${i}`;
            res.pair = f.pair;
            res.url = f.api_url;
            return res;
        })
        console.log(asTable(feeds_table) );

        console.log(`\n[Info]`.yellow, `pusher will start in ${CONF.interval_ms/1000} seconds`);
        if(!CONF.push_to_network){
            console.log(`\n*Test Mode*`.red, 'pushing to network disabled in config.json\n');
        }
        let message = 
        `**Oraclepusher v${pj.version} Started.**\n`+
        `Custodian: ${CONF.custodian}\n`+
        `Worker: ${this.worker}\n`+
        `Authorization: ${CONF.authorization.map(a => `${a.actor}@${a.permission}`).join(', ')}\n`
        this.notify(message);
    }

    notify(message){
        if(!this.notifiers.length) return;
        for(let i = 0; i < this.notifiers.length; i++){
            this.notifiers[i].send(message);
        }
    }

    load_plugins(){
        if(this.plugins.length == 0){
            console.log(`[Info]`.yellow, `No plugins loaded`);
            return;
        }
        console.log(`• Plugins `.underline.bold.bg);
        const tracePrototypeChainOf = (object) => {
            var proto = object.constructor.prototype;
            var result = [];
            while (proto) {
                if(proto.constructor.name != "Object"){
                    result.push(proto.constructor.name);
                }
                proto = Object.getPrototypeOf(proto)
            }
            return result;
        }

        for(let i=0; i < this.plugins.length; i++){
            let plugin = this.plugins[i];
            const pchain = tracePrototypeChainOf(plugin);
            switch (pchain[pchain.length - 1]) {
                case "Base_Notifier":
                    console.log(`*`.main, pchain[0]);
                    this.notifiers.push(plugin);
                    break;
            
                default:
                    console.log(`error loading plugin ${pchain.join('->')}`.red);
                    break;
            }

        }
    }

    async check_version(){
        let url = "https://gitlab.com/vigorstablecoin/oraclepusher/-/raw/master/package.json";
    
        const controller = new AbortController();
        const timeout = setTimeout(
          () => { controller.abort(); },
          5000,
        );
        let res;
        try {
            let response = await fetch(url, { signal: controller.signal });
            res = await response.json();
            if(pj.version != res.version){
                let msg = `new version detected v${res.version}, it's recommended to update the oraclepusher.`;
                console.log('[Warning]'.yellow, msg, "\n");
                this.notify(msg);
            }
            this.version_check_timestamp = Date.now();

        } 
        catch(err) {
            if (err.name == "AbortError") {
                //api request timeout
                console.log('[Version Check Warning]'.yellow, `Server responded too slow.`);
            }
            else {
                //other error
                console.log('[Version Check Warning]'.yellow, `Something went wrong`);
            }
        } 
        finally {
            clearTimeout(timeout);
        }
    }

}

module.exports = {
    Pusher
};