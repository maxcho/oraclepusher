const path = require('path');
require('dotenv').config({path: path.join(__dirname, '../.env')});
const {Base_Notifier} = require('./abstract/Base_Notifier');
const fetch = require("node-fetch");



class Telegram_Notifier extends Base_Notifier{

    constructor(telegramhandle){
        super();
        this.url = "http://161.35.205.209:3000/notify";
        
        if(!telegramhandle){
            throw new Error("No receiving telegram handle supplied in the constructor.");
        }
        this.receiver = telegramhandle.startsWith('@')? telegramhandle.substring(1) : telegramhandle;

        this.authorization = process.env.TELEGRAM_BOT_SECRET;
        if(!this.authorization){
            throw new Error("Secret to interact with the bot not found as environment variable.");
        }
        // console.log(this.authorization);
        // console.log(this.receiver);
    }

    async send(message){
        try{
            let res = await fetch(this.url, { 
                method: 'post', 
                headers: {
                  'Authorization': this.authorization, 
                  'Content-Type': 'application/json'
                }, 
                body: JSON.stringify({telegram: this.receiver, message: message })
            });
            if(res.ok){
                console.log("notification sent.")
                return true;
            }
            else{
                console.log("error sending notification.")
                return false;
            }
            
        }
        catch(e){
            return false;
        }

    }

}

module.exports = {
    Telegram_Notifier
};
