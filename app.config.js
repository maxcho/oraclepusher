const path = require('path');
require('dotenv').config({path: path.join(__dirname, './.env')});

//Plugins (uncomment to enable see plugins: [] )
const { Telegram_Notifier } = require("./plugins/Telegram_Notifier");


//edit config below
module.exports = {
  custodian: "piecesnbitss",
  worker: "piecesworker",
  private_key: process.env.ACCOUNT_PK,
  authorization: [{ actor: "piecesworker", permission: "oraclehub" }, { actor: "piecesnbitss", permission: "oraclehub" }],
  rpc_nodes: ["https://eos.greymass.com", "https://eosbp.atticlab.net", "https://api.eossweden.se"],

  plugins: [
    /*
    Uncomment the next line to enable telegram notifications via @VigorDACbot 
        - add your telegram handle in the constructor
        - ask for the telegram bot secret in the custodian channel and add it to your .env file (see example.env)
        - start a conversation with @VigorDACbot on telegram
    */
    new Telegram_Notifier("@kasperfish", process.env.TELEGRAM_BOT_SECRET)
  ],

  workers: [/*todo*/],

  //todo: need to be moved to workers config
  oraclehub_contract: "vigoraclehub",
  interval_ms: 60000,
  api_request_timeout_ms: 15000,
  push_to_network: false,
  telegram_handle: "@kasperfish",
};

