module.exports = {
  apps : [
    {
      name: 'OraclePusher',
      script: 'my_pusher.js',
      args: null,
      restart_delay:0,
    },
    {
      name: 'rpcProxyServer',
      script: 'rpcProxy.js',
      args: null,
      restart_delay:0,
    }
  ]
}
